<?php
class Controller{
    protected function model($model,$class)
    {
        require_once '../app/models/'.$model.'.php';
        return new $class();
    }
    protected function view($view,$parameters=[])
    {
        require_once '../app/views/'.$view.'.php';
    }
}
?>