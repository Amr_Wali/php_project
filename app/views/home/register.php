<!DOCTYPE html>
    <html lang="en">
    <head>

        <meta charset="UTF-8">
        <title> Register</title>
        <link rel="stylesheet" href="/PHP_Project/puplic/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>

    <nav class="navbar navbar-expand-sm fixed-top  " style="background-color: #20bbb3">
        <a href="index.html" class="navbar-brand">
            <img src="logo.png" id="logo" alt="logo" class="img-fluid " width="60" style="border:1px">
        </a>
        <form id="form" action="#" method="" class="form-inline">
            <input type="text" class="form-control text-left mr-2" placeholder="Search...">
        </form>
        <div class="collapse navbar-collapse text-right" id="collapsibleNavbar ">

            <ul class="navbar-nav ml-auto ">
                <li class="nav-item ">
                    <a href="#" class="nav-link text-white">Register</a>
                </li>
                <li class="nav-item">
                    <a href="login.html" class="nav-link text-white">Login</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white">AboutUs</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white ">ContactUs</a>
                </li>

            </ul>
        </div>

    </nav>

    <br>
    <br>


    <div class="container container-mobile p-t-md">

        <i class="fa fa-user mr-1"></i>

    <div class="text-center">
    <h3 style="color:#20bbb3"> <span class="fa fa-new-message mr-1" style="font-size:40px; color:#C0C0C0"></span> <br />Register</h3>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="panel-body">

         <form action="/PHP_Project/puplic/user/register" method="POST" class="form-horizontal" >

         <div class="form-group">
           <label class="col-md-3 control-label"  for="login_as">register as : </label>
                <select name="login_as" id="hideElement">
                    <option value="journalist">journalist</option>
                    <option value="user">client</option>
                </select>
        </div>          
            <div class="form-group">
            <label class="col-md-3 control-label" for="User_name">name</label>
            <div class="col-md-9" >
            <input type="text" placeholder=" Username" class="form-control" data-val="true"
                      data-val-required="A value is required." id="User_name" name="User_name" value="">
            </div>
            </div>
        <div class="form-group">
        <label class="col-md-3 control-label" for="Password">Password</label>
        <div class="col-md-9">
        <input class="form-control" type="password" data-val="true" data-val-length="Password length should be between 6 and 100 characters." data-val-length-max="100" data-val-length-min="6" data-val-required="A value is required." id="Password" name="Password" />
        <span class="text-danger field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label" for="ConfirmPassword">Password Confirmation</label>
        <div class="col-md-9">
        <input class="form-control" type="password" data-val="true" data-val-equalto="Password and it&#x27;s confimation don&#x27;t match." data-val-equalto-other="*.Password" id="ConfirmPassword" name="ConfirmPassword" />
        <span class="text-danger field-validation-valid" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
        </div>
    </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label" for="Email">Email</label>
        <div class="col-md-9">
        <input class="form-control" data-toggle="Email" title="Please input a valid e-mail" type="email" data-val="true" data-val-email="The Email field is not a valid e-mail address." data-val-required="A value is required." id="Email" name="Email" value="" />
        <span class="text-danger field-validation-valid" data-valmsg-for="Email" data-valmsg-replace="true"></span>
        </div>
    </div>


    <div class="form-group" id="hiddenItem" >
        <label class="col-md-3 control-label" for="Catogory">catogory</label>
        <div class="col-md-9">
        <select name="Catogory">
                    <option value="Social">Social</option>
                    <option value="Political">Political</option>
                    <option value="Economic">Economic</option>
                    <option value="Educational">Educational</option>
                    <option value="Sports">Sports</option>
                    <option value="International">International</option>
        </select>
        </div>
    </div>
    
    

  
    <div class="form-group">
        <div class="col-md-9 col-md-offset-3">
        <label><input type="checkbox" value="TRUE" class="checkbox-inline" checked data-val="true" data-val-required="The Notifications field is required." id="Notifications" name="Notifications" /> Notifications</label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-9 col-md-offset-3">
        <label><input id="TermsCB" type="checkbox" value="FALSE" class="checkbox-inline" /> I have read and accept the  <a target="_blank" href="/Home/Terms">Terms and Conditions</a></label>
        <span id="TermsValidation" class="text-danger"></span>
        </div>
    </div>
   
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit"  name="submit" id="RegisterBtn" class="btn btn-outline-info"><a href="#">Register</a></button>
        </div>
    </div>
  
    </form>

    </div>
    </div>


    </div>
    <br/>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="/PHP_Project/puplic/js/bootstrap.min.js"></script>

    </body>
    </html>


    
<!-- <?php 

// try {
//     $db=new pdo("mysql:host=127.0.0.1;dbname=EnewApp","mony","12345");
   
// }catch (PDOException $e){
// 	echo "Error!: " . $e->getMessage() . "<br/>";
// 	die();
// }
// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


	
    

//     if (isset($_POST['submit'])) {
// 		$info = ["User_name","Password","Email"];
// 		foreach ($info as $value) {
// 			if (empty($_POST[$value])) {
// 				header("Location:Register.php?error={$value}");
// 				die();
// 			}
// 		}
// 		$User_name=$_POST['User_name'];
//         $Password=$_POST['Password'];
//         $Email=$_POST['Email'];
	 	
	 	
// 	 	try {
// 		    $db->beginTransaction();
// 			$stmt = "INSERT INTO Users (User_name, Password, Email) VALUES (?,?,?)";
// 			$db->prepare($stmt)->execute([$User_name, $Password, $Email]);
			
			
// 			echo "aaa";
// 			$db->commit();
			
// 		} catch (PDOException $e){
// 			echo "Error!: " . $e->getMessage() . "<br/>";
// 			$db->rollback();
// 			die();
			
// 		}



//     }
    ?> -->
