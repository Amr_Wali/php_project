<?php
class User1 extends Model
{
    public $username;
    public $pass;
    public $status;
    public $email;
	
    public function add()
    {
        $sql = "INSERT INTO `Users`(`User_Id`, `User_Name`, `Password`, `Status`, `Email`) VALUES (Null,?,?,?,?)";
        $prep=$this->conn->prepare($sql);
        $query=$prep->execute([$this->username,$this->pass,$this->status,$this->email]);
        return $prep->rowcount();
    }

    public function log()
    {
        $sql = "SELECT * FROM `Users` WHERE `User_Name`=? and `Password`=? and `Status` = ?";
        $prep=$this->conn->prepare($sql);
        $query=$prep->execute([$this->username,$this->pass,$this->status]);
        return $prep->rowcount();
    }
}
 ?>