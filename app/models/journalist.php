<?php
class Journalist extends Model
{
    
    public $journalist_name;
    public $pass;
    public $status;
    public $category;
    public $email;
	
    public function add()
    {
        $checks=['JUser_Name'=>$this->journalist_name,'E'=>$this->email];
        foreach ($checks as $key => $value) {
            $stmt = $this->conn->prepare("SELECT * FROM Journalists WHERE {$key}=?");
            $stmt->execute([$value]);
            if ($stmt->fetchColumn()>0)
            {
                echo $key . " is already exist";
                break;
            }
        }
        $sql="insert into Journalists (JUser_Name,Password,Status,Catogry,Email) values (?,?,?,?,?)";
        $prep=$this->conn->prepare($sql);
 
        $query=$prep->execute([$this->journalist_name,$this->pass,$this->status,$this->category,$this->email]);
        return $prep->rowcount();
    }
 

    public function delete($id)
    {
        $stmt = $this->conn->query("SELECT * FROM Journalists WHERE JUser_Id ={$id}");
        if ($stmt->fetchColumn()>0)
        {
            $del=$this->conn->prepare("DELETE FROM Journalists WHERE JUser_Id =?");
            $del->execute([$id]);
            if($del->rowcount()==0){
                echo "Failed to delete";
            }
        }
        else
        {
            echo "This id doesn't exist";
        }
    }

    public function activate($id)
    {
        $stmt=$this->conn->query("UPDATE Journalists SET Status = 'Active' WHERE JUser_Id = {$id}");
    }

    public function pend($id)
    {
        $stmt=$this->conn->query("UPDATE Journalists SET Status = 'Pending' WHERE JUser_Id = {$id}");
    }
}
?>