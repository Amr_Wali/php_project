-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 31, 2019 at 09:03 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `EnewApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE `Comments` (
  `New_Id` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `Time` datetime NOT NULL,
  `Text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Images`
--

CREATE TABLE `Images` (
  `Image_Id` int(11) NOT NULL,
  `Extension` enum('.jpg','.gif','.png') NOT NULL,
  `New_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Journalists`
--

CREATE TABLE `Journalists` (
  `JUser_Id` int(11) NOT NULL,
  `JUser_Name` varchar(50) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Status` enum('Pending','Active') NOT NULL,
  `Catogry` enum('Social','Political','Economic','Educational','Sports','International') NOT NULL,
  `E` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Journalists`
--

INSERT INTO `Journalists` (`JUser_Id`, `JUser_Name`, `Password`, `Status`, `Catogry`, `E`) VALUES
(3, 'mshaaban', '12345', 'Active', 'Sports', 'MOHAMED@gmail.com'),
(4, 'amrwali', '12345', 'Active', 'Educational', 'amrwali@gmail.com'),
(5, 'mmona', '12345', 'Active', 'International', 'mmona@gmail.com'),
(6, 'momniya', '12345', 'Active', 'Economic', 'momnya@gmail');

-- --------------------------------------------------------

--
-- Table structure for table `News`
--

CREATE TABLE `News` (
  `New_Id` int(11) NOT NULL,
  `New_Name` varchar(300) NOT NULL,
  `Catogory` enum('Social','Political','Economic','Educational','Sports','International') NOT NULL,
  `Text` text NOT NULL,
  `Status` enum('Pending','Approved','Regicted','Deleted') NOT NULL,
  `Time` datetime NOT NULL,
  `JUser_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `User_Id` int(11) NOT NULL,
  `User_Name` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Status` enum('Pending','Active','Locked','') NOT NULL,
  `Email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`User_Id`, `User_Name`, `Password`, `Status`, `Email`) VALUES
(1, 'shaaban', '12345', 'Active', 'mezhd2013@gmail.com'),
(2, 'amr', '12345', 'Active', 'amr@gmail.com'),
(3, 'rihab', '12345', 'Active', 'rihab@gmail.com'),
(4, 'omnya', '12345', 'Active', 'omnya@gmail.com'),
(5, 'mona', '12345', 'Active', 'mona@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Comments`
--
ALTER TABLE `Comments`
  ADD PRIMARY KEY (`New_Id`,`User_Id`,`Time`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Indexes for table `Images`
--
ALTER TABLE `Images`
  ADD PRIMARY KEY (`Image_Id`),
  ADD KEY `New_Id` (`New_Id`);

--
-- Indexes for table `Journalists`
--
ALTER TABLE `Journalists`
  ADD PRIMARY KEY (`JUser_Id`),
  ADD UNIQUE KEY `JUser_Name` (`JUser_Name`),
  ADD UNIQUE KEY `E` (`E`),
  ADD KEY `JUser_Id` (`JUser_Id`);

--
-- Indexes for table `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`New_Id`),
  ADD UNIQUE KEY `New_Name` (`New_Name`),
  ADD KEY `JUser_Id` (`JUser_Id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`User_Id`),
  ADD UNIQUE KEY `User_Name` (`User_Name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Images`
--
ALTER TABLE `Images`
  MODIFY `Image_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Journalists`
--
ALTER TABLE `Journalists`
  MODIFY `JUser_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `News`
--
ALTER TABLE `News`
  MODIFY `New_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `User_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Comments`
--
ALTER TABLE `Comments`
  ADD CONSTRAINT `Comments_ibfk_2` FOREIGN KEY (`User_Id`) REFERENCES `Users` (`User_Id`),
  ADD CONSTRAINT `Comments_ibfk_3` FOREIGN KEY (`New_Id`) REFERENCES `News` (`New_Id`);

--
-- Constraints for table `Images`
--
ALTER TABLE `Images`
  ADD CONSTRAINT `Images_ibfk_1` FOREIGN KEY (`New_Id`) REFERENCES `News` (`New_Id`);

--
-- Constraints for table `News`
--
ALTER TABLE `News`
  ADD CONSTRAINT `News_ibfk_1` FOREIGN KEY (`JUser_Id`) REFERENCES `Journalists` (`JUser_Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
